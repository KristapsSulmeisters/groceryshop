public class Cart {
    int i;
    Product[] cartOfProducts;


    public Cart( int size) {
        cartOfProducts = new Product[size];

    }


    public Product[] getCartOfProducts() {
        return cartOfProducts;
    }

    public void setCartOfProducts(Product[] cartOfProducts) {
        this.cartOfProducts = cartOfProducts;
    }

    public void addToCart(Product p){

            cartOfProducts[i] = p;
            i++;

    }

    public double getCartPrice(){
        double sum = 0;
        for(int ind = 0 ; ind < cartOfProducts.length; ind++){
            sum = sum + cartOfProducts[ind].getPrice();
        }
        return sum;
    }


}
