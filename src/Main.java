import java.util.Scanner;

//add cart  add products to cart
public class Main {
    public static void main(String[] args) {

        Scanner keyboard = new Scanner(System.in);

        int i = 0;
        Cart cart = new Cart(5);
        while(i < 5) {

            Product p = new Product();
            cart.addToCart(p);
            System.out.println("Enter product name");
            p.setName(keyboard.next());


            System.out.println("Enter product price");
            p.setPrice(keyboard.nextDouble());


            i++;
            if(i == 5){
                System.out.println("Shopping is finished");
            }

        }
        System.out.println(cart.getCartPrice());
    }
}
